import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div>
        <div className="bg-dark">
            <div className="container">
                <p className="text-center text-white py-5">
                    Copyright © Your Website 2022
                </p>
            </div>
        </div>
      </div>
    )
  }
}
