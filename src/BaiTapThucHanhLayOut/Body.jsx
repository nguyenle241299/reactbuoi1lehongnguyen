import React, { Component } from 'react'

export default class Body extends Component {
  render() {
    return (
        <>    
            <div className='py-5'>
                <div className="container px-lg-5">
                    <div className="py-4 p-lg-5 bg-light text-center rounded-3">
                        <h1 className='display-5 fw-bold'>A warm welcome!</h1>
                        <p className="fs-5">
                            Bootstrap utility classes are used to create this jumbotron since the old component has been removed from the framework. Why create custom CSS when you can use utilities?
                        </p>
                        <a href="" className="btn btn-primary">Call to action</a>
                    </div>
                   
                   <div className="row mt-5">
                       <div className="col-4">
                            <div className="card">
                                <img className="card-img-top" src="https://techtopvn.com/wp-content/uploads/2018/10/hinh-anh-dep-ve-hoa-hong-nhung-3.jpg" height={200} alt="Card image" />
                                <div className="card-body">
                                    <h4 className="card-title">Hoa hồng</h4>
                                    <p className="card-text">Some example text.</p>
                                    <a href="#" className="btn btn-primary">See Profile</a>
                                </div>
                            </div>
                       </div>
                       <div className="col-4">
                            <div className="card">
                                <img className="card-img-top" src="https://kynguyenlamdep.com/wp-content/uploads/2020/03/hoa-hong-do-tuoi-500x344.jpg" height={200} alt="Card image" />
                                <div className="card-body">
                                    <h4 className="card-title">Hoa hồng</h4>
                                    <p className="card-text">Some example text.</p>
                                    <a href="#" className="btn btn-primary">See Profile</a>
                                </div>
                            </div>
                       </div>
                       <div className="col-4">
                            <div className="card">
                                <img className="card-img-top" src="https://tse4.mm.bing.net/th?id=OIP.aSIrlqQ_D35tQRLrBXryngHaE8&pid=Api&P=0" height={200} alt="Card image" />
                                <div className="card-body">
                                    <h4 className="card-title">Hoa hồng</h4>
                                    <p className="card-text">Some example text.</p>
                                    <a href="#" className="btn btn-primary">See Profile</a>
                                </div>
                            </div>
                       </div>
                   </div>
                </div>

            </div>
        </>
    )
  }
}
